var barra = document.getElementById("barra");
var texto = document.getElementById("texto");
var numero = document.getElementById("numero");
var dialogo = document.getElementById("dialogo_config");
var dialogoAudition = document.getElementById("dialogoAudition");
var dialogoGiftPin = document.getElementById("dialogoGiftPin");
var titulo_hwid = document.getElementById("titulo");
var contenido_dialogo = document.getElementById("contenido-dialogo");
var estadoWinmode = document.querySelector('.wincheck');
var estadoShader = document.querySelector('.shadercheck');
var estadoFPS = document.querySelector('.fpscheck');
var estadoResolucion = document.querySelector('.resolucioncheck');
var onConfig = document.getElementById("onConfig");
var botonPlay = document.getElementById("botonPlay");
var pin = document.getElementById("pin");
var serial = document.getElementById("serial");


eel.expose(titulo);
function titulo(texto_titulo) {
	titulo_hwid.innerHTML = titulo_hwid.innerHTML + " HWID: " + texto_titulo;
}

eel.expose(barra_aumento);
function barra_aumento(porcentaje) {
	barra.style.width = porcentaje+"%";
	let pink_position = 10000/porcentaje;
	barra.style.background = `linear-gradient(to right, skyblue, hotpink ${pink_position}%)`;
}

eel.expose(files_texto);
function files_texto(archivos) {
	texto.innerHTML = archivos;
}

eel.expose(files_cantidad);
function files_cantidad(cantidad) {
	numero.innerHTML = cantidad;
}

function abrir_dialogo() {
	botonPlay.disabled = true;
	dialogo.show();
}

function cerrar_dialogo() {
	valueEstadoWinmode = estadoWinmode.checked;
	valueEstadoWinmode = + valueEstadoWinmode;
	valueEstadoWinmode = valueEstadoWinmode.toString();

	valueEstadoShader = estadoShader.checked;
	valueEstadoShader = + valueEstadoShader;
	valueEstadoShader = valueEstadoShader.toString();

	valueEstadoResolucion = estadoResolucion.value;

	valueEstadoFPS = estadoFPS.value;

	configuracionAuLatin = {"valueEstadoWinmode":valueEstadoWinmode, 
							"valueEstadoShader":valueEstadoShader, 
							"valueEstadoResolucion":valueEstadoResolucion, 
							"valueEstadoFPS":valueEstadoFPS}

	eel.configuracion(configuracionAuLatin);
	botonPlay.disabled = false;
	dialogo.close();
}

eel.expose(abrirDialogoAudition);
function abrirDialogoAudition() {
	botonPlay.disabled = true;
	dialogoAudition.show();
}

function cerrarDialogoAudition(){
	botonPlay.disabled = false;
	dialogoAudition.close();
}

function abrirDialogoGiftPin(){
	botonPlay.disabled = true;
	dialogoGiftPin.show();
}

function cerrarDialogoGiftPin(){
	botonPlay.disabled = false;
	dialogoGiftPin.close();
}

eel.expose(cerrar_ventana);
function cerrar_ventana() {
	window.close();
}


eel.expose(configuracionSet)
function configuracionSet(valor){
	estadoWinmode.checked = valor["valor_winmode"];
	estadoShader.checked = valor["shader"];
	estadoResolucion.value = valor["resolucion"];
	estadoFPS.value = valor["fps_valor"];
}

eel.expose(onConfigPlay);
function onConfigPlay(){
	onConfig.onclick = abrir_dialogo;
}

eel.expose(activarBotonPlay);
function activarBotonPlay() {
	botonPlay.hidden = false;
}

eel.expose(setPinSerial);
function setPinSerial(pinValor, serialValor) {

	pin.innerHTML = pinValor;
	serial.innerHTML = serialValor;
}