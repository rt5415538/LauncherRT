# LauncherRT
Un mini launcher para el videojuego Audition Latino. Se creó con el objetivo de reducir la lenta carga en los discos HDD al momento de una actualización (o tratar XD).

![image](https://user-images.githubusercontent.com/57423251/275415900-588f62ae-bcc5-407a-9cf2-e50773d6b5d6.png)



# Advertencia
- El Launcher creará una carpeta llamada "RT_DATA" la cual guarda archivos importantes para su funcionamiento, por favor NO ELIMINAR.
- El Launcher solo hará una vez su "larga" comprobación, luego el launcher funcionará con normalidad, por favor NO SE ASUSTE.

# Navegadores Soportados
LauncherRT utiliza navegadores de internet como GUI, asi que asegúrese de usar uno de los siguientes navegadores:
- Google Chrome
- Microsoft Edge
- Brave


# Trucos de uso
Puedes editar el contenido de "data_no_update.txt" para omitir la comprobación de algunos archivos.

# Descarga
Puedes descargar el ejecutable desde [Mediafire](https://www.mediafire.com/folder/6m4z9360yugj2/LauncherRT)

# Convertir .PY a .EXE
Si no quieres descargar nada porque piensas que tiene virus, puedes crear tu propio .exe con este repositorio.

Para convertir de un .py a .exe necesitarás crear un entorno virtual en Python (o Anaconda) e instalar dentro de esta los siguientes paquetes:
- [auto-py-to-exe](https://github.com/brentvollebregt/auto-py-to-exe)
- [Eel](https://github.com/python-eel/Eel) (se tiene que reemplazar algunos archivos de la librería con los archivos de la carpete eel)
- [requests](https://pypi.org/project/requests/)
- [psutil](https://pypi.org/project/psutil/)
- [pymem](https://pypi.org/project/Pymem/)

Descargamos el repositorio y lo guardamos.

Una vez instalado los paquetes, nos dirijimos hacia la instalacion de las librerías de Python según tu "entorno virtual" y buscamos la carpeta eel, en mi caso:

![image](https://github-production-user-asset-6210df.s3.amazonaws.com/57423251/265596161-a68385e6-2bf0-4f30-91cd-812a18e63f13.png)

Una vez ubicado la carpeta, reemplazamos los archivos con los archivos de la carpeta eel de este repositorio

Abrimos auto-py-to-exe y seguimos los siguientes pasos:

![image](https://github-production-user-asset-6210df.s3.amazonaws.com/57423251/269477090-a9650903-d986-429b-a39a-d75d43937ca7.png)


- Localizamos en archivo .py
- Seleccionamos "Un Archivo" y "Consola de Windows(Ocultar Consola)"
- Seleccionamos el archivo logo.ico que se encuentra en la carpeta rt_web para el logo del launcher
- En "Archivos Adicionales" seleccionamos "añadir carpeta" y seleccionamos la carpeta rt_web
- Seleccionamos el directorio de salida (a su elección)
- CONVERTIR .PY TO .EXE


# Licencia
LauncherRT se publica bajo la licencia MIT que se encuentra en el archivo [LICENSE](LICENSE).
