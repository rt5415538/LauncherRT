import os
import sys

f = open(os.devnull, 'w')
sys.stdout = f
sys.stderr = f

import eel
import requests
import subprocess
import json
import hashlib
import zipfile
import math
import ctypes
import configparser
import winreg as reg
import psutil
from pymem import Pymem


@eel.expose
def cerrar_launcher():
    sys.exit()

@eel.expose
def configuracion(valor):
    
    winmode = valor["valueEstadoWinmode"]
    shader = valor["valueEstadoShader"]
    resolucion = valor["valueEstadoResolucion"].split("x")
    fps = valor["valueEstadoFPS"]
    
    auconfig = configparser.ConfigParser()
    auconfig.optionxform = str
    aufile = "Config.ini"
    auconfig.read(aufile)
    
    if "CONFIG" not in auconfig.keys():
        auconfig["CONFIG"] = {}
    
    auconfig["CONFIG"]["WINMODE"] = winmode
    auconfig["CONFIG"]["SHADER_SYSTEM"] = shader
    auconfig["CONFIG"]["RTFPS"] = fps
    auconfig["CONFIG"]["WIDTH"] = resolucion[0]
    auconfig["CONFIG"]["HEIGHT"] = resolucion[1]
    
    with open(aufile, "w") as auconfigfile:
        auconfig.write(auconfigfile, space_around_delimiters=False)
        
@eel.expose
def launchau():
    
    try:
        os.remove("t2embed.dll")
        
    except:
        pass
    
    aufile = "Config.ini"
    auconfig = configparser.ConfigParser()
    auconfig.optionxform = str
    auconfig.read(aufile)
    
    if "CONFIG" not in auconfig.keys():
        auconfig["CONFIG"] = {}
    
    shader = auconfig["CONFIG"].get("SHADER_SYSTEM", "1")
    fps_valor = auconfig["CONFIG"].get("RTFPS", "1")
    
    reg_thai()
    
    dll_fps_1 = "TaskKeyHookWD.dll"
    dll_fps_2 = "HighFPS-15.dll"
    dll_fps_3 = "HighFPS-16.dll"
    dll_truco = "t2embed.dll"
    
    fps_config = {1:[dll_fps_1, dll_fps_1], 2:[dll_fps_2, dll_truco], 3:[dll_fps_3, dll_truco]}
    
    selec_fps = fps_config[int(fps_valor)]
    
    exe = "Audition.exe"
    key = " /t3enter 15007D45626307077F5C463938635756352D775057"
    
    path_local = os.getcwd()+"\\"+exe
    pid_Audition = 0
    
    #VERIFICAMOS SI EL PROCESO DE AUDITION.EXE (LATINO) EXISTE
    
    for proceso in psutil.process_iter(attrs=['pid', 'name']):
        
        try:
            proceso_info = proceso.info
            proceso_name = proceso_info['name']
            
            # Cambia el filtro aquí
            if 'Audition' not in proceso_name:
                continue
            
            proceso_pid = proceso_info['pid']
            
            for modulo in psutil.Process(proceso_pid).memory_maps():
                
                if path_local.lower() in modulo.path.lower():
                    eel.abrirDialogoAudition()
                    return #se sale de la ejecución de Audition.exe
            
        except:
            pass
        
    os.rename(selec_fps[0], selec_fps[1])
    audition = subprocess.Popen(exe+key)
    pid_Audition = audition.pid
    audition_proceso = psutil.Process(pid_Audition)
    audition_proceso.suspend()
    
    #ACTIVAR KILL NOTICE, DESACTIVAR SHADER_FX
    
    pm = Pymem(pid_Audition)
    #pm.write_bool(kill_notice, False) #desactivado por no obtener nuevo address
    
    if not int(shader):
        
        shader_system = None

        while shader_system == None:
            audition_proceso.resume()
            eel.sleep(0.005)
            audition_proceso.suspend()
            shader_system = pm.pattern_scan_by_size_address(b'\x53\x68\x61\x64\x65\x72\x46\x58', 
                                                direccion = 0x5000000, 
                                                final_direccion=0x5f00000)
    
        pm.write_bytes(shader_system, b'\x52', 1)
        
        
    audition_proceso.resume()
    
    """
    if not int(shader):
        
        # Obtén el identificador del proceso (PID) del proceso objetivo
        process = ctypes.windll.kernel32.OpenProcess(
            ctypes.wintypes.DWORD(0x1F0FFF),  # Todos los permisos
            False,  # No heredar el manejador
            ctypes.wintypes.DWORD(pid_Audition)  # PID del proceso
        )
    
        # Dirección de memoria y tamaño de la región que deseas modificar
        address = shader_system  # Reemplaza con la dirección de memoria correcta
        size = ctypes.c_size_t(1)  # Tamaño de la región (en bytes)
    
        # Nuevos permisos de protección (por ejemplo, PAGE_EXECUTE_READWRITE)
        new_protect = ctypes.wintypes.DWORD(0x40)  # PAGE_EXECUTE_READWRITE
    
        # Parámetros de la función VirtualProtectEx
        old_protect = ctypes.wintypes.DWORD()  # Almacena los permisos originales
    
        # Llama a la función VirtualProtectEx para cambiar los permisos
        ctypes.windll.kernel32.VirtualProtectEx(
            process,  # Identificador del proceso
            ctypes.wintypes.LPVOID(address),  # Dirección de memoria
            size,  # Tamaño
            new_protect,  # Nuevos permisos
            ctypes.byref(old_protect)  # Almacena los permisos originales
        )
    
        pm.write_bytes(shader_system, b'\x52', 1)
        
        ctypes.windll.kernel32.VirtualProtectEx(
            process,  # Identificador del proceso
            ctypes.wintypes.LPVOID(address),  # Dirección de memoria
            size,  # Tamaño
            old_protect,  # Nuevos permisos
            ctypes.byref(new_protect)  # Almacena los permisos originales
        )
        
    """
    #SE BUSCA LA DLL textinputframework.dll YA QUE LA QUE CARGA DESPUES DE
    #T2EMBED.DLL
    
    dll_encontrado = False
    
    while not dll_encontrado:
        
        for modulo in psutil.Process(pid_Audition).memory_maps():
            
            if "textinputframework.dll" in modulo.path.lower():
                dll_encontrado = True
                break
    
    os.rename(selec_fps[1] , selec_fps[0]) #volver el dll a su nombre original
    
def reg_thai():
    
    reg_path = r'SOFTWARE\AUDITION\Thailand'
    install_type = reg.HKEY_CURRENT_USER
    
    try:
        reg_key = reg.OpenKey(install_type, reg_path, 0, reg.KEY_READ)
        key_thai_version = reg.QueryValueEx(reg_key, "VERSION")
        reg_key.Close()
        
    except WindowsError:
        reg_key = reg.CreateKey(install_type, reg_path)
        reg.SetValueEx(reg_key, "VERSION", 0, reg.REG_DWORD, 0)
        reg_key.Close()

@eel.expose
def main():
    
    #OBTENER HARDWARE ID Y MOSTAR EN EL LAUNCHER
    """
    hwid_pc = subprocess.check_output("wmic csproduct get uuid",creationflags=subprocess.CREATE_NO_WINDOW).split(b"\n")[1].strip()
    hwid_pc_texto = hwid_pc.decode()
    eel.titulo(hwid_pc_texto)
    """
    #VERIFICAR ESTADO DE WINMODE, RESOLUCION Y FPS
    
    aufile = "Config.ini"
    auconfig = configparser.ConfigParser()
    auconfig.optionxform = str
    auconfig.read(aufile)
    
    if "CONFIG" not in auconfig.keys():
        auconfig["CONFIG"] = {}
    
    valor_winmode = auconfig["CONFIG"].get("WINMODE", "0")
    shader = auconfig["CONFIG"].get("SHADER_SYSTEM", "1")
    fps_valor = auconfig["CONFIG"].get("RTFPS", "1")
    ancho_resolucion = auconfig["CONFIG"].get("WIDTH", "1024")
    alto_resolucion = auconfig["CONFIG"].get("HEIGHT", "768")

    config = {"valor_winmode": int(valor_winmode),
              "shader": int(shader),
              "fps_valor": fps_valor,
              "resolucion": ancho_resolucion+"x"+alto_resolucion
              }
    
    eel.configuracionSet(config)
    
    #ELIMINAR ANTIGUO LauncherRT_OLD.EXE SI ES QUE SE ACTUALIZÓ
    
    try:
        eel.sleep(0.2)
        os.remove("LauncherRT_OLD.exe")
        
    except Exception:
        pass
    
    
    
    #-1 DEFINICONES PARA DESCARGA Y OBTENCIÓN DE DATOS DE MEDIAFIRE
    
    def obtener_datos_mediafire(carpeta_mediafire):
        
        eel.barra_aumento(0)
        url_mediafire_data = "https://www.mediafire.com/api/1.5/folder/get_content.php?content_type=files&filter=all&order_by=name&order_direction=asc&chunk=1&folder_key={}&response_format=json"
        
        try:
            api = requests.get(url_mediafire_data.format(carpeta_mediafire))
            contenido_json = json.loads(api.content)
            lista_carpeta_mediafire = contenido_json["response"]["folder_content"]["files"]
            eel.barra_aumento(100)
            return lista_carpeta_mediafire
        
        except Exception:
            return 1
        
    def descargar_archivo(link, path_archivo, select_403 = 0):
        eel.barra_aumento(0)
        
        try:
            archivo = requests.get(link, stream = True, allow_redirects = True)
            
            if select_403 == 1:
                if archivo.status_code == 403:
                    return 3
                
            peso_archivo = int(archivo.headers.get("Content-Length"))
            division_archivo = math.ceil(peso_archivo/1000)
            parte_archivo_descargado = 0
            
        except Exception: #error de conexión
            return 1 
            
        try:
            
            with open(path_archivo, "wb") as data:
                
                for partes in archivo.iter_content(chunk_size=division_archivo):
                    data.write(partes)
                    parte_archivo_descargado += division_archivo
                    
                    if parte_archivo_descargado > peso_archivo:
                        parte_archivo_descargado = peso_archivo
                        
                    dato_barra = round((parte_archivo_descargado*100)/peso_archivo)
                    
                    eel.barra_aumento(dato_barra)
                    
        except Exception: #error de descarga
            return 2
    
    def obtener_hash(archivo,tipo_hash):
        
        with open(archivo, "rb") as file:
            
            for partes in iter(lambda: file.read(65536),b""):
                tipo_hash.update(partes)
                
        return tipo_hash.hexdigest()
    
    #0 REVISAR Y ACTUALIZAR LauncherRT
    
    #0.a OBTENER DATOS DE LauncherRT DE MEDIAFIRE
    
    carpeta_launcher_rt = "6m4z9360yugj2" #original
    eel.files_texto("Verificando actualizaciones de LauncherRT.exe...")
    lista_carpeta_mediafire_rt = obtener_datos_mediafire(carpeta_launcher_rt)
    
    if lista_carpeta_mediafire_rt == 1:
        eel.files_texto("Error al obtener los datos de mediafire LauncherRT.")
        return
    
    eel.files_texto("Datos de mediafire obtenidos.")
    
    #0.b VERIFICAR SI HAY ACTUALIZACION MEDIANTE HASH SHA256
    
    for x in lista_carpeta_mediafire_rt:
        nombre_rt = x["filename"]
        hash_web = x["hash"]
        
    nombre_rt_local = sys.executable
    hash_local = obtener_hash(nombre_rt_local, hashlib.sha256())
    
    #0.c SI EL HASH NO ES IGUAL, ENTONCES SE ACTUALIZA
    
    if hash_web != hash_local:
        
        #0.e DESCARGAR EL NUEVO LauncherRT.exe
        
        cantidad_archivos_actualizar = [0,len(lista_carpeta_mediafire_rt)]
        
        for cada_archivo in lista_carpeta_mediafire_rt:
            nombre_archivo = "LauncherRT_NEW.exe"
            path_archivo = ".\\"+nombre_archivo
            cantidad_archivos_actualizar[0] += 1
            mensaje_cantidad_archivos_actualizar = str(cantidad_archivos_actualizar[0])+"/"+str(cantidad_archivos_actualizar[1])
            eel.files_cantidad(mensaje_cantidad_archivos_actualizar)
            
            nuevo_link_mediafire_rt = "https://babygera10.pythonanywhere.com/static/launcherrt/LauncherRT.exe"
            
            eel.files_texto("Descargando {}.".format(nombre_archivo))
            
            resultado = descargar_archivo(nuevo_link_mediafire_rt, path_archivo)
            
            if resultado == 1:
                eel.files_texto("Error al conectarse con {}.".format(nombre_archivo))
                return
            
            elif resultado == 2:
                eel.files_texto("Error al descargar {}.".format(nombre_archivo))
                os.remove(nombre_archivo)
                return
        
        os.rename(nombre_rt_local, "LauncherRT_OLD.exe")
        os.rename(nombre_archivo, "LauncherRT.exe")
        subprocess.Popen(nombre_rt)
        eel.cerrar_ventana()
        
        
    
    #OBTENER PIN, SERIAL, ARCHIVOS NO ACTUALIZAR Y ADD DATA
    
    try:
        url_b4u_api = "https://babygera10.pythonanywhere.com/b4u"
        b4u_api = requests.get(url_b4u_api)
        b4u_api_contenido = json.loads(b4u_api.content)
        
    except:
        eel.files_texto("Error con la api de B4U")
        return
    
    archivos_no_actualizar = b4u_api_contenido["archivos_no_actualizar"]
    archivos_no_actualizar_rev_full = b4u_api_contenido["archivos_no_actualizar_rev_full"]
    add_data = b4u_api_contenido["add_data"]
    
    pin = b4u_api_contenido["giftpin"]["pin"]
    serial = b4u_api_contenido["giftpin"]["serial"]
    
    global kill_notice#, shader_system
    kill_notice = b4u_api_contenido["direcciones"]["KILL_NOTICE"]
    #shader_system = b4u_api_contenido["direcciones"]["SHADER_SYSTEM"]
    
    eel.setPinSerial(pin, serial)
    
        
    #1 VERIFICAR Y ACTUALIZAR CLIENTE DE AUDITION
    
    carpeta_rt = "RT_DATA"
    
    try:
        os.mkdir(carpeta_rt)
        
    except Exception:
        pass
    
    #1.a DESCARGAR CONTENT.JSON.ZIP Y DESCOMPRIMIRLO
    
    eel.files_cantidad("")
    eel.files_texto("Descargando datos del cliente Audition Latino...")
    contenido_zip = "content.json.zip"
    url_content = "http://audl.axeso5.com/update/index/{}".format(contenido_zip)
    
    resultado = descargar_archivo(url_content, contenido_zip)
    
    if resultado == 1:
        eel.files_texto("Error al conectarse con {}.".format(contenido_zip))
        return
    
    elif resultado == 2:
        eel.files_texto("Error al descargar {}.".format(contenido_zip))
        return
    
    with zipfile.ZipFile(contenido_zip, "r") as zip_content:
        contenido_json_audition = zip_content.namelist()[0]
        archivo_info = zip_content.getinfo(contenido_json_audition)
        content_crc32 = hex(archivo_info.CRC).replace("0x", "").upper()
        zip_content.extractall()
        
    os.remove(contenido_zip)
    
    #1.b OBTENER NOMBRES Y DATOS DE JSON WEB
    
    with open(contenido_json_audition, "r") as contenido:
        dic_contenido_json = json.load(contenido)
        
    archivos_audition = dic_contenido_json["files"]
    version_web = dic_contenido_json["version"]
    
    #1.c ARCHIVOS NO ACTUALIZAR Y ADD DATA
    
        #AÑADIR ADD_DATA A CONTENT
        
    archivos_audition.extend(add_data)
    
        #ARCHIVOS NO ACTUALIZAR LOCALES
    nombre_archivo_no_update = carpeta_rt + "\\data_no_update.txt"
    
    try:
        
        with open(nombre_archivo_no_update,"r") as file_no_update:
            archivos_no_verificar = file_no_update.readlines()
            archivos_no_verificar = [x.replace("\n", "") for x in archivos_no_verificar]
            archivos_no_actualizar.extend(archivos_no_verificar)
            archivos_no_actualizar = list(set(archivos_no_actualizar))
    
    except Exception:
        archivos_no_verificar = ["macro.txt", "DATA\\svol.dat"]
        
        with open(nombre_archivo_no_update,"w") as file_no_update:
            for x in archivos_no_verificar:
                file_no_update.write(x+"\n")
    
    #1.d COMPROBAR SI CONTENT_RT.JSON LOCAL EXISTE PARA ACTUALIZAR ARCHIVOS
    
    nuevo_archivos_audition_local = {}
    nombre_contenido_json_local = "content_rt.json"
    lista_archivos_actualizar = []
    cantidad_archivos_actualizar = [0,0]
    guardar_archivo_json = False
    
    if os.path.isfile(carpeta_rt+"\\"+nombre_contenido_json_local):
    
        #1.e ABRIR Y OBTENER DATOS JSON LOCAL
        
        with open(carpeta_rt+"\\"+nombre_contenido_json_local, "r") as contenido:
            dic_contenido_json_local = json.load(contenido)
            
        archivos_audition_local = dic_contenido_json_local["files"]
        version_local = dic_contenido_json_local["version"]
            
        #1.f VERIFICAR ARCHIVOS A ACTUALIZAR
        
        if version_web == version_local:
            
            for archivo in archivos_audition_local:
                nombre_archivo_local = archivo["localName"]
                eel.files_texto("Comprobando {}".format(nombre_archivo_local))
                #peso_archivo_web = int(archivo["remoteSize"])
                web_hash_md5 = archivo["localMd5"]
                nombre_archivo_web = archivo["remoteName"]
                _, extension = os.path.splitext(nombre_archivo_web)
                
                
                try:
                    if nombre_archivo_local not in archivos_no_actualizar:
                        local_peso = int(archivo["localSize"])
                        """
                        if extension == ".zip":
                            local_hash_md5 = obtener_hash(nombre_archivo_local, hashlib.md5())
                            
                            if local_hash_md5 != web_hash_md5:
                                lista_archivos_actualizar.append(archivo)
                                cantidad_archivos_actualizar[1] += 1
                            
                        else:
                            peso_archivo_local = os.path.getsize(nombre_archivo_local)
                            
                            if peso_archivo_local != peso_archivo_web:
                                lista_archivos_actualizar.append(archivo)
                                cantidad_archivos_actualizar[1] += 1
                        """
                        
                        peso_archivo_local = os.path.getsize(nombre_archivo_local)
                        
                        if peso_archivo_local != local_peso:
                            lista_archivos_actualizar.append(archivo)
                            cantidad_archivos_actualizar[1] += 1
                        
                except Exception:
                    lista_archivos_actualizar.append(archivo)
                    cantidad_archivos_actualizar[1] += 1
                    
        else:
            
            #1.g ORDENAR ARCHIVOS_AUDITION_LOCAL PARA MEJOR LECTURA (LLAVE DE LLAVES)
            
            for archivo in archivos_audition_local:
                nombre_archivo = archivo["localName"]
                #del archivo["localName"]
                nuevo_archivos_audition_local[nombre_archivo] = archivo
                
            #1.h COMPROBAR JSON WEB CON JSON LOCAL Y OBTENER ARCHIVOS ACTUALIZAR
            
            for archivo in archivos_audition:
                nombre_archivo_local = archivo["localName"]
                web_hash_md5 = archivo["localMd5"]
                eel.files_texto("Comprobando {}".format(nombre_archivo_local))
                
                if nombre_archivo_local not in archivos_no_actualizar:
                
                    try:
                        local_hash_md5 = nuevo_archivos_audition_local[nombre_archivo_local]["localMd5"]
                        local_peso = nuevo_archivos_audition_local[nombre_archivo_local]["localSize"]
                        archivo["localSize"] = str(local_peso)
                        
                        
                        if web_hash_md5 != local_hash_md5:
                            lista_archivos_actualizar.append(archivo)
                            cantidad_archivos_actualizar[1] += 1
                        
                    except Exception:
                        lista_archivos_actualizar.append(archivo)
                        cantidad_archivos_actualizar[1] += 1
                        
            guardar_archivo_json = True
        
    #1.i CONTENT_RT_JSON NO EXISTE, COMPROBAR CON HASH A TODO ARCHIVO
        
    else:
        
        #1.j COMPROBAR HASH A TODO
        
        for archivo in archivos_audition:
            nombre_archivo_local = archivo["localName"]
            web_hash_md5 = archivo["localMd5"]
            eel.files_texto("Comprobando {}".format(nombre_archivo_local))
            
            if nombre_archivo_local not in archivos_no_actualizar_rev_full:
                
                try:
                    local_hash_md5 = obtener_hash(nombre_archivo_local, hashlib.md5())
                    local_peso = os.path.getsize(nombre_archivo_local)
                    archivo["localSize"] = str(local_peso)
                    
                    if local_hash_md5 != web_hash_md5:
                        lista_archivos_actualizar.append(archivo)
                        cantidad_archivos_actualizar[1] += 1
                        
                except Exception:
                    lista_archivos_actualizar.append(archivo)
                    cantidad_archivos_actualizar[1] += 1
        
        guardar_archivo_json = True
        
    #1.k ACTUALIZAR ARCHIVOS
    
    url_archivos = "http://audl.axeso5.com/update/content/{}"
    url_archivos_respaldo = "https://babygera10.pythonanywhere.com/static/audition/{}"
    
    for cada_archivo in lista_archivos_actualizar:
        nombre_archivo = cada_archivo["localName"]
        web_file = cada_archivo["remoteName"]
        ruta_archivo = os.path.join(os.getcwd(), nombre_archivo)
        ruta_archivo = os.path.dirname(ruta_archivo)
        _, extension = os.path.splitext(web_file)
        
        cantidad_archivos_actualizar[0] += 1
        mensaje_cantidad_archivos_actualizar = str(cantidad_archivos_actualizar[0])+"/"+str(cantidad_archivos_actualizar[1])
        eel.files_cantidad(mensaje_cantidad_archivos_actualizar)
        eel.files_texto("Descargando {}".format(nombre_archivo))
        
        if not os.path.isdir(ruta_archivo):
            os.makedirs(ruta_archivo, exist_ok=True)
            
        resultado = descargar_archivo(url_archivos.format(web_file), web_file, select_403 = 1)
        
        if resultado == 1:
            eel.files_texto("Error al conectarse con {}.".format(nombre_archivo))
            return
            
        elif resultado == 2:
            eel.files_texto("Error al descargar {}.".format(nombre_archivo))
            return
            
        elif resultado == 3:
            eel.files_texto("Descarga alternativa de {}".format(nombre_archivo))
            
            resultado = descargar_archivo(url_archivos_respaldo.format(web_file), web_file)
            
            if resultado == 1:
                eel.files_texto("Error al conectarse con {}.".format(nombre_archivo))
                return
                
            elif resultado == 2:
                eel.files_texto("Error al descargar {}.".format(nombre_archivo))
                return
        
        if extension == ".zip":
            
            with zipfile.ZipFile(web_file, "r") as zip_file:
                zip_file.extractall(ruta_archivo)
                
            os.remove(web_file)
        
        if guardar_archivo_json:
            local_peso = os.path.getsize(nombre_archivo)
            cada_archivo["localSize"] = str(local_peso)
    
        
    #1.l COPIAR Y ELIMINAR CONTENIDO_JSON_AUDITION, CREAR ARCHIVO .A5
    
    if guardar_archivo_json:
        with open(carpeta_rt+"\\"+nombre_contenido_json_local, "w") as file:
            json.dump(dic_contenido_json, file)
            
    file_nombre_a5 = "{}_{}.a5".format(content_crc32, version_web)
    a5_extension = ".a5"
    
    if not os.path.isfile(file_nombre_a5):
        
        for file in os.listdir():
            if file.endswith(a5_extension):
                os.remove(file)
    
        with open(file_nombre_a5, "w") as f:
            pass
        
    os.remove(contenido_json_audition)
        
    eel.files_texto("Actualización del cliente de Audition Latino finalizado.")

    #9 ACTUALIZACION FINALIZADA
    
    eel.files_cantidad("")
    eel.onConfigPlay()
    eel.activarBotonPlay()

def can_use_chrome():
    chrome_instance_path = eel.chrome.find_path()
    return chrome_instance_path is not None and os.path.exists(chrome_instance_path)

def can_use_edge():
    edge_instance_path = eel.edge.find_path()
    return edge_instance_path is not None and os.path.exists(edge_instance_path)

def can_use_brave():
    brave_instance_path = eel.brave.find_path()
    return brave_instance_path is not None and os.path.exists(brave_instance_path)
"""
def can_use_opera():
    opera_instance_path = eel.opera.find_path()
    return opera_instance_path is not None and os.path.exists(opera_instance_path)
"""
eel.init("rt_web", allowed_extensions=[".js",".html"])
html = "rt.html"
resolucion = (816, 489)

#OBTENER RESOLUCION DE PANTALLA
user32 = ctypes.windll.user32
user32.SetProcessDPIAware()
ancho, alto = user32.GetSystemMetrics(0), user32.GetSystemMetrics(1)

#SI LA PANTALLA TIENE SCALA, SE OBTIENE PARA OBTENER RESOLUCION RESULTANTE
scaleFactor = ctypes.windll.shcore.GetScaleFactorForDevice(0) / 100

eje_x = ((ancho/scaleFactor) - resolucion[0])/2
eje_y = ((alto/scaleFactor) - resolucion[1])/2

posicion = (int(eje_x), int(eje_y))

if can_use_chrome():
    #eel.spawn(main)
    eel.start(html, size=resolucion, position=posicion, port=0, mode="chrome")
    """
elif can_use_opera():
    eel.start(html, port=0, mode="opera")
    """
elif can_use_brave():
    #eel.spawn(main)
    eel.start(html, size=resolucion, position=posicion, port=0, mode="brave")
    
elif can_use_edge():
    #eel.spawn(main)
    eel.start(html, size=resolucion, position=posicion, port=0, mode="edge")
    
else:
    #eel.spawn(main)
    eel.start(html, port=0, mode="user default")