import platform
import subprocess as sps
import sys
import os
from typing import List

from eel.types import OptionsDictT

name = 'Brave'


def run(_path, options, start_urls):
    cmd = 'start brave --incognito --app={}'.format(start_urls[0])
    sps.Popen(cmd, stdout=sys.stdout, stderr=sys.stderr, stdin=sps.PIPE, shell=True)

def find_path():
    if sys.platform in ['win32', 'win64']:
        return _find_brave_win()
    
    else:
        return None

def _find_brave_win():
    import winreg as reg
    reg_path = r'SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\brave.exe'

    for install_type in reg.HKEY_CURRENT_USER, reg.HKEY_LOCAL_MACHINE:
        try:
            reg_key = reg.OpenKey(install_type, reg_path, 0, reg.KEY_READ)
            brave_path = reg.QueryValue(reg_key, None).replace('"',"")
            reg_key.Close()
            if not os.path.isfile(brave_path):
                continue
        except WindowsError:
            brave_path = None
        else:
            break

    return brave_path